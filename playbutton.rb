#Play button img 

class PlayButton
        attr_accessor :image

        def initialize
                @image = Gosu::Image.new("img/play_button.png")
        end
        
        def draw
                @image.draw(37, 440, 0)
        end     
end
