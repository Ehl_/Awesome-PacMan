load 'template.rb'

class Ghost
        def initialize(color) 
                @template = Template.new
                @color = color
                if color == "red" 
                        @image = @template.image.subimage(1233, 561, 36, 36) 
                        @x = 263
                        @y = 265
                elsif color == "pink"
                        @image = @template.image.subimage(1233, 636, 36, 36)
                        @x = 303
                        @y = 280
                elsif color == "blue"
                        @image = @template.image.subimage(1233, 711, 36, 36)
                        @x = 223
                        @y = 280
                end
        end

        def draw 
                @image.draw(@x, @y, 0)
        end     
end