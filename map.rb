load 'template.rb'

class Map
	def initialize
		@template = Template.new
		@image = @template.image.subimage(40, 40, 561, 635)
	end

	def draw
		@image.draw(0, 0, 0)
	end
end