load 'map.rb'
load 'ghost.rb'
load 'pacman.rb'
load 'playbutton.rb'


class Window < Gosu::Window
	def initialize
		super 561, 800
		self.caption = "Awesome-PacMan"
		@map = Map.new
		@ghostRed = Ghost.new("red")
		@ghostPink = Ghost.new("pink")
		@ghostBlue = Ghost.new("blue")
		@pacman = Pacman.new
		@playbutton = PlayButton.new
	end

	def update
		@pacman.update_direction
	end

	def draw
		@map.draw
		@ghostRed.draw
		@ghostPink.draw
		@ghostBlue.draw
		@pacman.draw
		@playbutton.draw
	end

	def needs_cursor?
		true
	end
end

