load 'template.rb'
load 'direction.rb'

class Pacman
	def initialize
		@pixel_pos_x = 1233
		@pixel_pos_x_tmp = 1233
		@pixel_pos_x_max = 1390
		@pixel_pos_y = [77, 150, 222, 295]
		@pixel_frame = 52
		@side = 33
		@direction = DIRECTION::UP
		@x = 264
		@y = 335
		@speed = 5
		@template = Template.new
		@image = @template.image.subimage(@pixel_pos_x, @pixel_pos_y[@direction], @side, @side)
	end

	def update_direction
		if Gosu.button_down? Gosu::KB_UP or Gosu::button_down? Gosu::GP_UP
			@direction = DIRECTION::UP
		end
		if Gosu.button_down? Gosu::KB_RIGHT or Gosu::button_down? Gosu::GP_RIGHT
			@direction = DIRECTION::RIGHT
		end
		if Gosu.button_down? Gosu::KB_DOWN or Gosu::button_down? Gosu::GP_DOWN
			@direction = DIRECTION::DOWN
		end
		if Gosu.button_down? Gosu::KB_LEFT or Gosu::button_down? Gosu::GP_LEFT
			@direction = DIRECTION::LEFT
		end
	end

	def next_frame
		@pixel_pos_x += @pixel_frame
		if @pixel_pos_x > @pixel_pos_x_max
			@pixel_pos_x = @pixel_pos_x_tmp
		end
		@image = @template.image.subimage(@pixel_pos_x, @pixel_pos_y[@direction], @side, @side)
	end

	def draw
		if @direction == DIRECTION::UP
			@y -= @speed
		end
		if @direction == DIRECTION::RIGHT
			@x += @speed
		end
		if @direction == DIRECTION::DOWN
			@y += @speed
		end
		if @direction == DIRECTION::LEFT
			@x -= @speed
		end
		@image.draw(@x, @y, 0)
		next_frame
		sleep(0.05)
	end
end